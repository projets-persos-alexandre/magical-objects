// #region Import Required External Modules

const express = require('express')
const logger = require('morgan')

// #endregion

// #region app variables
const app = express()
const port = 3000

app.set('port', port)

// #endregion
// #region App Configuration

// Inisialize Morgan
app.use(logger('dev'))

// Inisialize body-parser
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

// #endregion

// #region API initialization

// get all models
const models = require('./models')

// get routes of API
const apiRoutes = require('./routes')
apiRoutes(app)

app.get('*', (req, res) => {
  res.status(200).send({
    message: 'welcome to the fezgse',
  })
})

// #endregion

// #region Server Action
models.sequelize.sync().then(() => {
  app.listen(port, () => {
    console.log('serveur lancé a http://localhost:' + port)
  })
})

// #endregion
