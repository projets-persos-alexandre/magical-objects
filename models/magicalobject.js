'use strict'
const { Model, DataTypes } = require('sequelize')
module.exports = (sequelize) => {
  class MagicalObject extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  MagicalObject.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Name can't be empty",
          },
          len: {
            args: [0, 150],
            msg: "name can't be longer than 150 characters",
          },
        },
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: "Description can't be empty",
          },
        },
      },
      rarity: {
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [0, 50],
            msg: "rarity can't be longer than 50 characters",
          },
        },
      },
      type: {
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [0, 50],
            msg: "type can't be longer than 50 characters",
          },
        },
      },
      source: {
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [0, 10],
            msg: "source can't be longer than 10 characters",
          },
        },
      },
      needsLink: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: 'MagicalObject',
    }
  )
  return MagicalObject
}
