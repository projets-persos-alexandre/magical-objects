'use strict'
const Sequelize = require('sequelize')
module.exports = {
  up: async (queryInterface) => {
    await queryInterface.createTable('MagicalObjects', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      description: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      rarity: {
        type: Sequelize.STRING(50),
      },
      type: {
        type: Sequelize.STRING(50),
      },
      source: {
        type: Sequelize.STRING(10),
      },
      needsLink: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('MagicalObjects')
  },
}
