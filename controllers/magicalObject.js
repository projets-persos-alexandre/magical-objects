const MagicalObject = require('../models').MagicalObject

module.exports = {
  create(req, res) {
    const magicalObject = MagicalObject.build({
      name: req.body.name,
      description: req.body.description,
      rarity: req.body.rarity,
      type: req.body.type,
      source: req.body.source,
    })
    magicalObject
      .validate()
      .then(() => {
        MagicalObject.create(magicalObject.dataValues)
          .then((newMagicalObject) => {
            return res.status(201).json(newMagicalObject)
          })
          .catch(() => {
            return res.status(500).json({ msg: 'internal server error' })
          })
      })
      .catch((error) => {
        return res.status(500).json({ msg: 'internal server error', error })
      })
  },
  get(req, res) {
    MagicalObject.findAll()
      .then((magicalObjects) => {
        return res.status(200).json(magicalObjects)
      })
      .catch(() => {
        return res.status(500).json({ msg: 'internal server error' })
      })
  },
  retrieve(req, res) {
    MagicalObject.findOne({ where: { id: req.params.id } })
      .then((magicalObject) => {
        if (magicalObject) {
          return res.status(200).json(magicalObject)
        } else {
          return res.status(404).json({ msg: 'Magical Object not found' })
        }
      })
      .catch(() => {
        return res.status(500).json({ msg: 'internal server error' })
      })
  },
  update(req, res) {
    const payload = {}
    if (req.body.name !== undefined) {
      payload.name = req.body.name
    }
    if (req.body.description !== undefined) {
      payload.description = req.body.description
    }
    if (req.body.rarity !== undefined) {
      payload.rarity = req.body.rarity
    }
    if (req.body.type !== undefined) {
      payload.type = req.body.type
    }
    if (req.body.source !== undefined) {
      payload.source = req.body.source
    }
    const magicalObject = MagicalObject.build(payload)

    magicalObject
      .validate({
        fields: Object.keys(req.body),
      })
      .then(() => {
        MagicalObject.update(payload, {
          where: {
            id: req.params.id,
          },
        })
          .then(() => {
            MagicalObject.findOne({
              where: {
                id: req.params.id,
              },
            })
              .then((magicalObject) => {
                if (magicalObject) {
                  return res.status(200).json(magicalObject)
                } else {
                  return res
                    .status(404)
                    .json({ msg: 'Magical Object not found' })
                }
              })
              .catch(() => {
                return res.status(500).json({ msg: 'Internal server error' })
              })
          })
          .catch(() => {
            return res.status(500).json({ msg: 'Internal server error' })
          })
      })
      .catch((err) => {
        return res.status(400).json({ msg: 'Data not valid', error: err })
      })
  },
  delete(req, res) {
    MagicalObject.destroy({
      where: {
        id: req.params.id,
      },
    })
      .then((magicalObjects) => {
        return res.status(204).json()
      })
      .catch(() => {
        return res.status(500).json({ msg: 'internal server error' })
      })
  },
}
