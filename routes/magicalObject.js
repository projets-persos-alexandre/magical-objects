const magicalObjectController = require('../controllers').magicalObject

module.exports = (app) => {
  app.post('/api/magicalObject', magicalObjectController.create)
  app.get('/api/magicalObject', magicalObjectController.get)
  app.get('/api/magicalObject/:id', magicalObjectController.retrieve)
  app.delete('/api/magicalObject/:id', magicalObjectController.delete)
  app.patch('/api/magicalObject/:id', magicalObjectController.update)
}
